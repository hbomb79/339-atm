<?php
/*
 ** 159.339 - Internet Programming - Assignment 1
 ** Harry Felton - 18032692
 ** Sam Liew     - 15349956
 *
 * Main index.php file, handles file reading, transaction application and error reporting via HTML.
 *
 */

namespace HarrySam\A1;

// Require our PSR-4 autoloader so classes are automatically imported from our source tree
require 'autoloader.php';

/**
 * Reads account information from the data/acct.txt file
 *
 * Reads the acct.txt file, parsing for data relevant to the Account objects (ID and balance)
 * If a line found does not contain this information, it is ignored. If a line does contain this information
 * an Account object is instantiated with it and is stored inside the $accounts array (which is returned). To retrieve
 * this information, use the ID of the account as they key for the array.
 *
 * @return array An array of Account objects where each key is the ID of it's value.
 */
 
function readAccounts() {
    /* Load account information */
    $accounts = [];

    // Construct our path with a OS environment-specific directory separator
    $path = sprintf("data%sacct.txt", DIRECTORY_SEPARATOR);

    // Open the file for reading
    $handle = fopen( $path,"r");
    if( !$handle ) {
        // Unable to open file acct.txt; PHP requires CHMOD of at least 0775 to read files (and the file must exist)
        die("Unable to open file " . $path ." - Check read permissions and that the file exists. Aborting!");
    }

    // Iterate over the $handle, reading a line each time.
    // For each line read, check that it doesn't equal false (no more lines)
    // Once we're out of lines, the loop will terminate.
    while( ( $line = fgets( $handle ) ) !== false ) {
        if( sscanf($line, "%d %f", $id, $balance) ) {
            // We read a line and parsed out an int and float in to $id and $balance.
            // Create a new Account instance holding the information we read, and insert it in to the $accounts array
            $accounts[ $id ] = new Account( $id, $balance );
        }
    }

    // We're done; close the file handle, freeing up this resource
    fclose( $handle );

    // Return all the accounts found.
    return $accounts;
}

/**
 * Reads transaction information from the data/tranz.txt file
 *
 * Reads the tranz.txt file, parsing for data relevant to transactions (id, type, and balance)
 * If a line found does not contain this information, it is ignored. If a line does contain this information
 * it is is stored inside the $transactions array (which is returned).
 *
 * @return array An array of arrays of format [int ID, char type, float balance, int lineNumber]
 */
 
function readTransactions() {
    /* Load transaction details */
    $transactions = [];

    // Construct the path with an OS environment-specific directory separator
    $path = sprintf("data%stranz.txt", DIRECTORY_SEPARATOR);

    // Open the file for reading
    $handle = fopen( $path, "r" );
    if( !$handle ) {
        // Unable to read file
        die("Unable to open file " . $path . " - Check read permissions and that the file exists. Aborting!");
    }


    // Read each line of the file, keeping track of which line we're on for
    // transaction error reporting; we could use the index of the array that the transaction
    // was reported on, however this would need to be manually incremented for the transaction file
    // header, and also means transactions that are malformed are not counted. Doing it this way ensures an accurate
    // line number.
    $lineNumber = 0;
    while( ( $line = fgets( $handle ) ) !== false ) {
        // Check if the scan was successful. Will fail for malformed lines (silently)
        // Will also fail for the header line.
        if( sscanf( $line, "%d %c %f", $id, $type, $amount ) ) {
            array_push($transactions, [$id, $type, $amount, $lineNumber]);
        }

        // Increment the line number each time. Transactions should start at line #1 because
        // the tranz.txt file has a file header which is not parsed.
        $lineNumber++;
    }

    // Close the file
    fclose( $handle );

    // Return our transactions
    return $transactions;
}

/**
 * Updates account information post-transaction to update.txt file
 *
 * After having run the transactions, the account objects are serialised
 * and written to the update.txt file in the same format that they were written (ID BALANCE)
 *
 * @param array $accounts A key-value array of account objects where the key is the ID of the value
 * @return void
 */
 
function updateAccounts( $accounts ) {
    /*
     * Having completed the transactions, write the new account information to file at 'update.txt'
     */

    $handle = fopen( 'update.txt', 'w' );
    if( !$handle ) {
        // If we can't open this file, it means PHP cannot access it or is unable to create it.
        // This error is outside of our control; it indicates that PHP does not have the required privileges to write
        // or open files. On Linux, CHMOD permissions of at-least 0777 are required on the directory to allow
        // PHP to open files.

        // Tell the user account information has not been updated, and then return out of the function
        // so we don't crash trying to write to a NULL file handle.
        ?>

        <!-- HTML error display, file cannot be opened -->
        <p style='color: red; margin: 1rem;'>
            <b>[CRITICAL] Unable to open file 'update.txt' for writing.</b>
            <br>Please check that the program has write permissions for this file/directory (at least 0777 CHMOD access privileges).
            <br><br>Account data has <b>NOT BEEN SAVED!</b>
        </p>

        <?php return;
    }

    // For each account, iterate and simply write the account to file (followed by a newline).
    // fwrite will automatically convert $acct to a string, calling it's __toString method
    foreach( $accounts as $acct ) {
        if( !fwrite( $handle, $acct . "\n" ) ) {
            echo "[WARNING] An error occurred while writing account information to 'update.txt'. File was opened successfully but writing failed. Permissions may have been revoked or file was deleted. update.txt shows a PARTIAL RECORD. Aborting file writing.";
            break;
        }
    }

    fclose( $handle );
}

/**
 * Apply transactions supplied to the accounts supplied.
 *
 * Given a list of transactions and accounts, iterates through each transaction applying each one
 * to the target account object (hence the $accounts array is a key-value pair to allow for easy
 * random access).
 * If a transaction is invalid, this function uses a try-catch block to catch the
 * BadTranzException and create a BadTranz object detailing the transaction that failed.
 *
 * This function returns an array of BadTranz objects (empty array if all successful).
 *
 * @param array $transactions An array containing arrays of structure [int, string, float] describing transactions
 * @param array $accounts A KEY-VALUE array of Account objects where the KEY is the account ID
 * @return array An array of bad transactions we encountered (or empty array if all succeeded)
 */
function applyTransactions( $transactions, $accounts ) {
    // Store BadTranz objects in here to return later
    $transactions_errors = [];

    // Iterate over each transaction, applying the transaction to the correct account from $accounts.
    for( $i = 0; $i < count( $transactions ); $i++ ) {
        // Surround in try-catch so we can catch any BadTranzExceptions that are thrown when
        // applying transactions to an Account.
        try {
            // $current_transaction is an array of [account_id, type, amount, lineNumber]
            $current_transaction = $transactions[$i];

            // Attempt to apply the transaction to the account
            // If this isset check fails, it means there is nothing in the $accounts array at the key checked
            // indicating the ID this transaction is targeted at doesn't correspond to an Account.
            if( isset( $accounts[$current_transaction[0]] ) ) {
                // Apply the transaction. If the transaction is invalid, a BadTranzException
                // will be thrown and caught in the catch block below.
                $accounts[$current_transaction[0]]->applyTransaction( $current_transaction[1], $current_transaction[2] );
            } else {
                // Account doesn't exist, throw a BadTranzException which will be caught in the catch
                // block below and converted to a BadTranz object.
                throw new BadTranzException("Invalid Account ID", 1);
            }
        } catch (BadTranzException $ex ) {
            // The transaction failed because the transaction was invalid.
            // We know what ACCOUNT we were working on, and the details of the transaction.
            // Use this information to create a new BadTranz object with the
            // line number, account ID, type of transaction, amount and reason (from $ex)
            $tranz_error = new BadTranz( $current_transaction[3], $current_transaction[0], $current_transaction[1],
                                         $current_transaction[2], $ex->getMessage(), $ex->getCode() );

            // Push the transaction error to the errors array.
            array_push( $transactions_errors, $tranz_error );
        }
    }

    // Return our array of transaction errors (empty if none).
    return $transactions_errors;
}

/* Main Program Flow */

// First, read in transactions and accounts
$transactions = readTransactions();
$accounts = readAccounts();

// Then apply transactions to accounts, receiving an array of BadTranz objects if any failed (otherwise empty array);
$transactions_errors = applyTransactions( $transactions, $accounts );

// Now that the transactions are complete. Save the new account information in the same format to 'update.txt'
updateAccounts( $accounts );

// Finally, display our HTML, using these two variables for neatness and to avoid counting arrays multiple
// times for no reason.
$tranz_count = count( $transactions );
$invalid_tranz_count = count( $transactions_errors );

?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>159.339 - A1 - ATM</title>
        <style>
            td,th {
                outline: solid 1px black;
                padding: 0.3rem;
                text-align: center;
            }

            tr.bad-transaction.acc-id td.acc-id,
            tr.bad-transaction.tranz-type td.tranz-type,
            tr.bad-transaction.tranz-amount td.tranz-amount {
                color: red;
                font-weight: bolder;
            }
        </style>
    </head>
    <body>
        <b>There were <?= $tranz_count ?> transactions in total.</b>
        <br><br>
        <b><?= $invalid_tranz_count == 0 ? 'All ('. $tranz_count .')' : $tranz_count - $invalid_tranz_count ?> transactions were valid.</b>
        <br><br>
        <?php if( $invalid_tranz_count > 0 ): ?>
            <b>See below for a table of invalid transactions</b>
            <table id="bad-transactions">
                <tbody>
                    <tr>
                        <th>Line #</th>
                        <th>ID</th>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Reason</th>
                    </tr>
                    <?php foreach( $transactions_errors as $bad_tranz ): ?>
                        <tr class="bad-transaction <?= BadTranz::CODE_ENUM[ $bad_tranz->getCode() ] ?>">
                            <td class="line"><?= $bad_tranz->getLineNumber() ?></td>
                            <td class="acc-id"><?= $bad_tranz->getAccountID() ?></td>
                            <td class="tranz-type"><?= $bad_tranz->getTransactionType() ?></td>
                            <td class="tranz-amount"><?= $bad_tranz->getTransactionAmount() ?></td>
                            <td class="reason"><?= $bad_tranz->getReason() ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </body>
</html>
