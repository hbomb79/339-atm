<?php
/*
 ** 159.339 - Internet Programming - Assignment 1
 ** Harry Felton - 18032692
 ** Sam Liew     - 15349956
 *
 * PSR-4 compliant auto-loader adjusted from provided git blob
 * https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md
 *
 */

namespace HarrySam\A1;

// Register new SPL autoloader.
spl_autoload_register(function($class){
    // Directory prefix for this project.
    $prefix = 'HarrySam\\A1\\';

    // The base dir for source files in this project (classes/exceptions).
    $base_dir = __DIR__ . '/src/';

    // Check if the class being required is actually trying to load from our namespace
    $len = strlen( $prefix );
    if( strncmp( $prefix, $class, $len ) ) {
        // If not, return and allow another registered autoloader to attempt loading.
        return;
    }

    // Now that we're certain the file being auto-loaded is intended for our namespace,
    // REMOVE the namespace from the string so we just have the relative class path.
    $relative_class = substr( $class, $len );

    // Then, prepend the base directory for the project to the relative class name;
    // We also clean up the class directory separators by removing any backwards slashes and replacing
    // them with the OS specific DIRECTORY_SEPARATOR
    $file = str_replace("\\", DIRECTORY_SEPARATOR, $base_dir . $relative_class) . '.php';

    // Check if the file path we've ended up with exists
    if( file_exists( $file ) ) {
        // Require it if so
        require $file;
    }

    // Otherwise, end execution and allow another autoloader to try.
});
