<?php
/*
 ** 159.339 - Internet Programming - Assignment 1
 ** Harry Felton - 18032692
 ** Sam Liew     - 15349956
 *
 * Account PHP class file, contains information about each account
 * Notably, current balance and account ID number.
 *
 * This class will also handle all transactions applied to it, throwing
 * a BadTranzException if the transaction is invalid, or the account funds
 * do not allow for a transaction to be carried out.
 *
 * These exceptions are then captured by the index.php file, as per the assignment specification.
 * Once the exception is caught, an instance of BadTranz is created detailing the reason for the
 * exception being rejected.
 *
 * At the end of execution, index.php will display a count of total transactions, valid transactions, and a table
 * of failed transactions.
 */

namespace HarrySam\A1;


class Account {
    protected $ID;
    protected $balance;

    /**
     * Account constructor.
     * @param $ID
     * @param $balance
     */
    public function __construct($ID, $balance) {
        $this->ID = $ID;
        $this->balance = $balance;
    }

    /**
     * Applies a transaction to this account
     *
     * Given the type of transaction ($type), and the amount of funds to manipulate ($amount), this function
     * applies a transaction to this account. If the transaction is invalid for any reason, a BadTranzException
     * is raised.
     *
     * @param string $type The type of transaction, either D (deposit) or W (withdrawal)
     * @param float $amount Amount of funds being manipulated
     * @throws BadTranzException Thrown if transaction type invalid, if account has insufficient funds, or if the amount specified is invalid (<0).
     */
    public function applyTransaction( $type, $amount ) {
        // Check if the transaction is valid. First test if the $amount is a positive float.
        if( $amount < 0 ) {
            throw new BadTranzException("Negative transaction amount", 2);
        }

        // Next, test the $type of the transaction. If valid, attempt to perform the transaction
        if( $type == 'W' ) {
            // If we're withdrawing funds, check that the balance allows for this
            $newBalance = $this->balance - $amount;
            if( $newBalance >= 0 ) {
                // Account has enough funds, update balance to reflect this.
                $this->balance = $newBalance;
            } else {
                throw new BadTranzException("Insufficient Funds", 2);
            }
        } elseif( $type == 'D' ) {
            // Deposits require no checking; we already know the $amount is >0, so just add the amount to the account
            // balance and call it a day.
            $this->balance += $amount;
        } else {
            throw new BadTranzException("Invalid transaction type. Only W/D permitted.", 3);
        }
    }


    /**
     * Converts the account object in to a string
     *
     * Concatenates the ID and balance together with a string in between. This matches the format of the
     * acct.txt file, and can therefore be used when writing the account objects to the update.txt file.
     *
     * @return string
     */
    public function __toString() {
        return sprintf("%d %.2f", $this->ID, $this->balance);
    }

}