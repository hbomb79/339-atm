<?php
/*
 ** 159.339 - Internet Programming - Assignment 1
 ** Harry Felton - 18032692
 ** Sam Liew     - 15349956
 *
 * BadTranz PHP class. When a BadTranzException is caught by index.php, a BadTranz object is created
 * and is used to store the details of the transaction that failed. Instead of stopping execution, the program
 * will continue to process all transactions, displaying a list of any failed transactions at the end
 *
 * This object is crucial to the program being able to display information on faulty transactions. It stores
 * details such as the line number where the transaction was found (in data/tranz.txt), and the reason why the
 * transaction was invalid.
 *
 */

namespace HarrySam\A1;


class BadTranz {
    /*
     * When displaying the HTML results, we use this array to map numeric values to
     * a string value. This string value is then used as a class on the table row for this BadTranz. This allows
     * us to style the particular part of the transaction that was invalid differently to the rest.
     */
    const CODE_ENUM = [
        1 => "acc-id", // Account ID was invalid
        2 => "tranz-amount", // Transaction amount was invalid
        3 => "tranz-type" // Transaction type was invalid
    ];

    protected $lineNumber;
    protected $accId;
    protected $tranzType;
    protected $amount;
    protected $reason;
    protected $errorCode;

    /**
     * BadTranz constructor.
     * @param int $line_number The line from which the faulty transaction was sourced
     * @param int $acc_id ID of the account the transaction was applied to
     * @param string $tranz_type The type of transaction, only D/W valid (char)
     * @param float $amount Amount of funds the transaction attempted to manipulate
     * @param string $reason The reason (human readable) that the transaction failed
     * @param int $code An error code describing the reason the transaction failed in a machine readable way. Used with CODE_ENUM.
     */
    public function __construct( $line_number, $acc_id, $tranz_type, $amount, $reason, $code ) {
        $this->lineNumber = $line_number;
        $this->accId = $acc_id;
        $this->tranzType = $tranz_type;
        $this->amount = $amount;
        $this->reason = $reason;
        $this->errorCode = $code;
    }

    /**
     * Returns the account ID the transaction was put to
     *
     * @return int
     */
    public function getAccountID() {
        return $this->accId;
    }

    /**
     * Returns the line number of the tranz.txt file where this transaction was found
     *
     * @return int
     */
    public function getLineNumber() {
        return $this->lineNumber;
    }

    /**
     * Returns the type of transaction
     *
     * @return string
     */
    public function getTransactionType() {
        return $this->tranzType;
    }

    /**
     * Returns the amount of money the transaction was trying to manipulate
     *
     * @return float
     */
    public function getTransactionAmount() {
        return $this->amount;
    }

    /**
     * Returns a human readable reason as to why the transaction failed
     *
     * @return string
     */
    public function getReason() {
        return $this->reason;
    }

    /**
     * Returns a machine-readable reason as to why the transaction failed
     *
     * This code can be used to determine WHY the transaction failed by the program.
     ** 1: Account ID invalid
     ** 2: Transaction amount was invalid (negative transaction amount or account had insufficient funds)
     ** 3: Transaction type was invalid (anything other that W/D is invalid)
     *
     * @return int
     */
    public function getCode() {
        return $this->errorCode;
    }
}