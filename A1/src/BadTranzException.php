<?php
/*
 ** 159.339 - Internet Programming - Assignment 1
 ** Harry Felton - 18032692
 ** Sam Liew     - 15349956
 *
 * BadTranzException PHP class. Thrown by the Account class when an invalid transaction is applied to an account.
 * This exception must be handled by index.php - program execution must not halt (recoverable exception).
 *
 * No details on the transaction itself are contained here, as when the exception is raised index.php will already
 * have that information at it's disposal. Also, exception classes are not usually used as a way to carry data, the only
 * exception (hah, get it?) being the reason for the exception being thrown.
 *
 */


namespace HarrySam\A1;

use Exception;

class BadTranzException extends Exception {

    /**
     * BadTranzException constructor.
     * @param string $message The message indicating the reason the transaction failed (human readable)
     * @param int $code Error code, used here to indicate which part of the transaction was invalid {see BadTranz}
     */
    public function __construct($message, $code){
        parent::__construct($message, $code);
    }
}